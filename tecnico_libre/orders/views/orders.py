"""Circle views."""

# Django REST Framework
from rest_framework import mixins, viewsets

# Permissions
from rest_framework.permissions import IsAuthenticated
from tecnico_libre.users.permissions import IsAccountOwner, IsWorker

# Filters
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

# Serializers
from tecnico_libre.orders.serializers import OrderModelSerializer

# Models
from tecnico_libre.orders.models import Order


class OrdersViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    """Circle view set."""

    serializer_class = OrderModelSerializer
    lookup_field = 'description'

    # Filters
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    search_fields = ('description',)
    ordering_fields = ('created', 'modified',)
    ordering = ('-created', )
    filter_fields = ('status',)

    def get_queryset(self):
        """Restrict list to public-only."""
        queryset = Order.objects.all()
        if self.action == 'list':
            return queryset.filter(status=1)
        return queryset

    def get_permissions(self):
        """Assign permissions based on action."""
        permissions = [IsAuthenticated, IsAccountOwner]
        if self.action in ['drop']:
            permissions.append(IsWorker)
        if self.action in ['take']:
            permissions.append(IsWorker)
        return [permission() for permission in permissions]

    # def perform_create(self, serializer):
    #     """Assign circle admin."""
    #     order = serializer.save()
    #     user = self.request.user
    #     profile = user.profile
    #     Membership.objects.create(
    #         user=user,
    #         profile=profile,
    #         circle=circle,
    #         is_admin=True,
    #         remaining_invitations=10
    #     )
