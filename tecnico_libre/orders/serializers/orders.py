"""Circle serializers."""

# Django REST Framework
from rest_framework import serializers

# Model
from tecnico_libre.orders.models import Order


class OrderModelSerializer(serializers.ModelSerializer):
    """Circle model serializer."""

    class Meta:
        """Meta class."""

        model = Order
        fields = (
            'description', 'subdescription',
            'status', 'direction',
        )


