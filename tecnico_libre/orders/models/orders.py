"""Profile model."""

# Django
from django.db import models

# models
from tecnico_libre.utils.models import TLBaseModel
from tecnico_libre.users.models import Client, Worker
# Choices
from tecnico_libre.orders.choices import ORDER_STATUS_TYPES, ORDER_OPEN


class Order(TLBaseModel):
    """Order model.
    An order connect user and worker for a job
    """

    client = models.ForeignKey(Client, on_delete=models.CASCADE,
                               verbose_name='Client than open the orders.', null=True)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE, null=True,
                               verbose_name='Worker than take the orders.')

    description = models.TextField(verbose_name="Order's description", blank=False)
    subdescription = models.TextField(verbose_name="Order's subdescription", blank=False)
    direction = models.TextField(verbose_name="Order's direction", blank=False)

    status = models.PositiveSmallIntegerField(
        choices=ORDER_STATUS_TYPES,
        default=ORDER_OPEN,
        verbose_name="estado de la orden"
    )

    def __str__(self):
        """Return user's str representation."""
        return str(self.user)
