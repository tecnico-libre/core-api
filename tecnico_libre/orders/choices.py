ORDER_OPEN = 1
ORDER_TAKED = 2
ORDER_CLOSED = 3
ORDER_APPEALED = 4

ORDER_STATUS_TYPES = (
    (ORDER_OPEN, 'abierta'),
    (ORDER_TAKED, 'tomada'),
    (ORDER_CLOSED, 'cerrada'),
    (ORDER_APPEALED, 'apelada'),
)