"""Orders URLs."""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# Views
from .views import orders as orders_views

router = DefaultRouter()
router.register(r'orders', orders_views.OrdersViewSet, basename='orders')
# router.register(
#     r'circles/(?P<slug_name>[-a-zA-Z0-0_]+)/members',
#     membership_views.MembershipViewSet,
#     basename='membership'
# )

urlpatterns = [
    path('', include(router.urls))
]
