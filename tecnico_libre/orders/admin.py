"""User models admin."""

# Django
from django.contrib import admin

# Models
from tecnico_libre.orders.models import Order


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    """Profile model admin."""

    list_display = ('subdescription',)
    # search_fields = ('user__username', 'user__email', 'user__first_name', 'user__last_name')
    # list_filter = ('reputation',)