"""Celery tasks."""

# Django
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone

# Models
from tecnico_libre.users.models import User
# from cride.rides.models import Ride

# Celery
from celery.decorators import task, periodic_task

# Utilities
import jwt
import time
from datetime import timedelta


def gen_verification_token(user, type):
    """Create JWT token that the user can use to verify its account."""
    exp_date = timezone.now() + timedelta(days=3)
    payload = {
        'user': user.username,
        'exp': int(exp_date.timestamp()),
        'type': type
    }
    token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')
    return token.decode()


@task(name='send_confirmation_email', max_retries=3)
def send_confirmation_email(user_pk):
    """Send account verification link to given user."""
    user = User.objects.get(pk=user_pk)
    verification_token = gen_verification_token(user, 'email_confirmation')
    subject = 'Bienvenido @{}! verifica tu cuenta para empezar a usar tu cuenta'.format(user.username)
    from_email = 'TecnicoLibre <noreply@tecnicolibre.com>'
    content = render_to_string(
        'emails/users/account_verification.html',
        {'token': verification_token, 'user': user}
    )
    msg = EmailMultiAlternatives(subject, content, from_email, [user.email])
    msg.attach_alternative(content, "text/html")
    msg.send()

@task(name='worker_confirmation_email', max_retries=3)
def worker_confirmation_email(user_pk):
    """Send account verification link to given user."""
    user = User.objects.get(pk=user_pk)
    verification_token = gen_verification_token(user, 'worker_confirmation')
    subject = 'Bienvenido @{}! Verifica tu cuenta como trabajador'.format(user.username)
    from_email = 'Tecnico Libre <noreply@tecnicolibre.com>'
    content = render_to_string(
        'emails/users/worker_verification.html',
        {'token': verification_token, 'user': user}
    )
    msg = EmailMultiAlternatives(subject, content, from_email, [user.email])
    msg.attach_alternative(content, "text/html")
    msg.send()


# @periodic_task(name='disable_finished_rides', run_every=timedelta(minutes=20))
# def disable_finished_rides():
#     """Disable finished rides."""
#     now = timezone.now()
#     offset = now + timedelta(minutes=20)

#     # Update rides that have already finished
#     rides = Ride.objects.filter(
#         arrival_date__gte=now,
#         arrival_date__lte=offset,
#         is_active=True
#     )
#     rides.update(is_active=False)
