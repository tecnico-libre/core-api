ORDERS = 1
USERS = 2
PAYMENTS = 3

HISTORY_TYPES = (
    (ORDERS, 'Orden'),
    (USERS, 'Usuario'),
    (PAYMENTS, 'Pago'),
)

CREATE = 1
TAKE = 2
DROP = 3
CLOSE = 4
RECHARGE_CREDITS = 5
REMOVE_CREDITS = 6
DELETE = 7
UPDATE = 8

ACTION_TYPES = (
    (CREATE, 'create'),
    (TAKE, 'take'),
    (DROP, 'drop'),
    (CLOSE, 'close'),
    (RECHARGE_CREDITS, 'recharge credits'),
    (REMOVE_CREDITS, 'remove credits'),
    (DELETE, 'delete'),
    (UPDATE, 'update'),
)
