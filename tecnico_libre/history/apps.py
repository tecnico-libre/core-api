from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class HistoryConfig(AppConfig):
    name = "tecnico_libre.history"
    verbose_name = _("History")

    def ready(self):
        try:
            import tecnico_libre.history.signals  # noqa F401
        except ImportError:
            pass
