"""History model."""

# Django
from django.db import models

# models
from tecnico_libre.utils.models import TLBaseModel

# chioces
from tecnico_libre.history.choices import HISTORY_TYPES, ACTION_TYPES


class History(TLBaseModel):
    """History model.
    Record actions around the platform
    """

    message = models.TextField(default='')
    user_id = models.CharField(max_length=30, null=False)
    data_id = models.CharField(max_length=30, null=False)
    action = models.PositiveSmallIntegerField(
        choices=ACTION_TYPES,
        null=False
    )
    module = models.PositiveSmallIntegerField(
        choices=HISTORY_TYPES,
        null=False
    )

    def __str__(self):
        """Return user's str representation."""
        return str(self.message)
