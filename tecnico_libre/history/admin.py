"""User models admin."""

# Django
from django.contrib import admin

# Models
from tecnico_libre.history.models import History


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin):
    """Profile model admin."""

    list_display = ('user_id', 'data_id', 'message', 'module')
    # search_fields = ('user__username', 'user__email', 'user__first_name', 'user__last_name')
    # list_filter = ('reputation',)
