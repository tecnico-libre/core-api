"""Users views."""

# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

# Permissions
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from tecnico_libre.users.permissions import IsAccountOwner

# Serializers
from tecnico_libre.users.serializers.profiles import ProfileModelSerializer
from tecnico_libre.users.serializers import (AccountVerificationSerializer,
                                             WorkerVerificationSerializer,
                                             UserLoginSerializer,
                                             UserModelSerializer,
                                             UserSignUpSerializer,
                                             UsernameSerializer)
from tecnico_libre.orders.serializers import OrderModelSerializer

# Models
from tecnico_libre.users.models import User
from tecnico_libre.orders.models import Order
from tecnico_libre.users.models import Client

# Tasks
from tecnico_libre.taskapp.tasks import worker_confirmation_email


class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    """User view set.
    Handle sign up, login and account verification.
    """

    queryset = User.objects.filter(is_active=True)
    serializer_class = UserModelSerializer
    lookup_field = 'username'

    def get_permissions(self):
        """Assign permissions based on action."""
        if self.action in ['signup', 'login', 'verify', 'username']:
            permissions = [AllowAny]
        elif self.action in ['retrieve', 'update', 'partial_update', 'profile']:
            permissions = [IsAuthenticated, IsAccountOwner]
        else:
            permissions = [IsAuthenticated]
        return [p() for p in permissions]

    @action(detail=False, methods=['post'])
    def login(self, request):
        """User sign in."""
        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        data = {
            'user': UserModelSerializer(user).data,
            'access_token': token
        }
        return Response(data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def signup(self, request):
        """User sign up."""
        serializer = UserSignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        data = UserModelSerializer(user).data
        return Response(data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def verify(self, request):
        """Account verification."""
        serializer = AccountVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        data = {'message': 'Congratulation, now go create some orders!'}
        return Response(data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def workerrequest(self, request):
        """Account verification."""
        worker_confirmation_email.delay(user_pk=request.user.id)
        return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def workerverification(self, request):
        """Account verification."""
        serializer = WorkerVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        data = {'message': 'Congratulation, now go take some orders!'}
        return Response(data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['put', 'patch'])
    def profile(self, request, *args, **kwargs):
        """Update profile data."""
        user = self.get_object()
        profile = user.profile
        partial = request.method == 'PATCH'
        serializer = ProfileModelSerializer(
            profile,
            data=request.data,
            partial=partial
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        data = UserModelSerializer(user).data
        return Response(data)

    @action(detail=False, methods=['post'])
    def username(self, request, *args, **kwargs):
        serializer = UsernameSerializer(data=request.data)
        response = {"is_allow": serializer.is_valid()}

        return Response(response, status=status.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        """Add extra data to the response."""
        response = super(UserViewSet, self).retrieve(request, *args, **kwargs)
        client = Client.objects.get(user=request.user)
        orders = Order.objects.filter(
            client=client,
        )
        data = {
            'user': response.data,
            'orders': OrderModelSerializer(orders, many=True).data
        }
        response.data = data
        return response
