"""Profile model."""

# Django
from django.db import models

# Utilities
from tecnico_libre.utils.models import TLBaseModel


class Client(TLBaseModel):
    """Profile model.
    A profile holds a user's public data like biography, picture,
    and statistics.
    """

    user = models.ForeignKey('users.User', on_delete=models.CASCADE)

    orders_open = models.PositiveIntegerField(default=0)

    reputation = models.FloatField(
        default=5.0,
        help_text="Client's reputation based on the orders completes by the workers."
    )

    def __str__(self):
        """Return user's str representation."""
        return str(self.user)
