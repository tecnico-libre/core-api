"""Worker model."""
# Django
from django.db import models

# Models
from tecnico_libre.utils.models import TLBaseModel
from tecnico_libre.users.models import User


class Worker(TLBaseModel):
    """Profile model.
    A profile holds a user's public data like biography, picture,
    and statistics.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)

     # Stats
    orders_taken = models.PositiveIntegerField(default=0)
    orders_closed = models.PositiveIntegerField(default=0)
    orders_droped = models.PositiveIntegerField(default=0)

    reputation = models.FloatField(
        default=5.0,
        help_text="User's reputation based on the orders completes."
    )
    
    def __str__(self):
        """Return user's str representation."""
        return str(self.user)