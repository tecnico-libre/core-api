"""User permissions."""

# Django REST Framework
from rest_framework.permissions import BasePermission


class IsAccountOwner(BasePermission):
    """Allow access only to objects owned by the requesting user."""

    def has_object_permission(self, request, view, obj):
        """Check obj and user are the same."""
        return request.user == obj

class IsWorker(BasePermission):
    """Allow access only to objects workers registered."""

    def has_object_permission(self, request, view, obj):
        """Check obj and user are the same."""
        return request.user.is_worker 

class IsOrdersWorker(BasePermission):
    """Allow access only to objects workers registered."""

    def has_object_permission(self, request, view, obj):
        """Check obj and user are the same."""
        return request.user.is_worker 

class IsOrdersCreator(BasePermission):
    """Allow access only to objects workers registered."""

    def has_object_permission(self, request, view, obj):
        """Check obj and user are the same."""
        return request.user == obj 
