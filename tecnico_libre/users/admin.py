"""User models admin."""

# Django
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Models
from tecnico_libre.users.models import User, Profile, Client, Worker


class CustomUserAdmin(UserAdmin):
    """User model admin."""

    list_display = ('email', 'username', 'first_name', 'last_name', 'is_client', 'is_verified', 'is_worker')
    list_filter = ('is_staff', 'created', 'modified', 'is_verified')

    readonly_fields = (
        'is_verified',
        'email',
        'google_id',
    )

    fieldsets = (
        ('Datos Personales', {
            'fields': (
                'first_name',
                'last_name',
                'email',

            )
        }),
        ('Cuenta', {
            'fields': (
                'is_verified',
                'is_staff',
                'is_superuser',
                'is_client',
                'is_worker',
                'google_id'
            )
        }),
    )


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """Profile model admin."""

    list_display = ('user','username')
    search_fields = ('user__username', 'user__email', 'user__first_name', 'user__last_name')
    # list_filter = ('reputation',)
    def username(self, obj):
        return obj.user.username
    username.short_description = 'username'

@admin.register(Worker)
class WorkerAdmin(admin.ModelAdmin):
    """Worker model admin."""

    # list_display = ('user',)
    # list_display = ('user__email', 'user__first_name', 'user__last_name')
    search_fields = ('user__username', 'user__email', 'user__first_name', 'user__last_name')
    readonly_fields = ('user','reputation', 'orders_taken', 'orders_droped', 'orders_closed')



@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    """Client model admin."""

    list_display = ('user', 'reputation')
    search_fields = ('user__username', 'user__email', 'user__first_name', 'user__last_name')
    list_filter = ('reputation',)
    readonly_fields = ('user','reputation', 'orders_open')

admin.site.register(User, CustomUserAdmin)
